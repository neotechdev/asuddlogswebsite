import React, { useState } from 'react';
import InputPane from './InputPane';
import LogsTable from './LogsTable';

export default function Page() {
    const [id, setId] = useState();

    return (
        <>
            <InputPane setDetectorId={setId}/>
            <LogsTable id={id}/>
        </>
    )
}
