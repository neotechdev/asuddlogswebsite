import React, { useState } from 'react';
import { TextField, Button } from '@material-ui/core';
import PropTypes from 'prop-types';

export default function InputPane(props) {
    const [id, setId] = useState();

    const handleChange = (event) => {
        setId(event.target.value)
    }

    const getLogs = () => {
        props.setDetectorId(id);
    }

    return (
        <div className='inputPane'>
            <TextField className='input' id="standard-basic" label="ID светофорного объекта" onChange={handleChange}/>
            <Button className='btn' variant="contained" color="primary" disabled={id ? false : true} onClick={getLogs}>Найти</Button>
        </div>
    )
}

InputPane.propTypes = {
    setDetectorId: PropTypes.func.isRequired,
}
    
