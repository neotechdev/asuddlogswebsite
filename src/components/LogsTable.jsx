import React, { useState, useEffect } from 'react';
import { DataGrid } from '@material-ui/data-grid';
import axios from 'axios';
import moment from 'moment';
import PropTypes from 'prop-types';
import dictionary from '../dictionary';

// for test
const PeekPlLogs = [
    {
      "trafficLightObjectId": "5c7a7f2c-1ef5-49fe-9555-b81e2fd4db2f",
      "date": "28.06.2021",
      "time": "14:38:00",
      "number": 1,
      "state": "X1-S6:УПРАВЛЕНИЕ",
      "typeStateId": "39acc89e-bd3f-4c0a-9a98-9a99f318e612"
    },
    {
      "trafficLightObjectId": "5c7a7f2c-1ef5-49fe-9555-b81e2fd4db2f",
      "date": "28.06.2021",
      "time": "14:38:00",
      "number": 2,
      "state": "X1-S5:КРУГОМ КРАСНЫЙ",
      "typeStateId": "39acc89e-bd3f-4c0a-9a98-9a99f318e612"
    },
    {
      "trafficLightObjectId": "5c7a7f2c-1ef5-49fe-9555-b81e2fd4db2f",
      "date": "28.06.2021",
      "time": "14:38:00",
      "number": 3,
      "state": "PLAN 2",
      "typeStateId": "39acc89e-bd3f-4c0a-9a98-9a99f318e612"
    },
    {
      "trafficLightObjectId": "5c7a7f2c-1ef5-49fe-9555-b81e2fd4db2f",
      "date": "28.06.2021",
      "time": "14:38:00",
      "number": 4,
      "state": "X1-S6:УПРАВЛЕНИЕ",
      "typeStateId": "39acc89e-bd3f-4c0a-9a98-9a99f318e612"
    },
    {
      "trafficLightObjectId": "5c7a7f2c-1ef5-49fe-9555-b81e2fd4db2f",
      "date": "28.06.2021",
      "time": "14:38:00",
      "number": 5,
      "state": "X1-S5:КРУГОМ КРАСНЫЙ",
      "typeStateId": "39acc89e-bd3f-4c0a-9a98-9a99f318e612"
    }
];
const PeekStlampLogs = [
  {
    "trafficLightObjectId": "5c7a7f2c-1ef5-49fe-9555-b81e2fd4db2f",
    "date": "28.06.2021",
    "time": "14:31:57",
    "number": 1,
    "name": "T1R1",
    "state": "OK",
    "alarm": 0,
    "amperage": 1,
    "voltage": 1,
    "power": 107,
    "calibre": 0,
    "level1": 5,
    "level2": 0,
    "typeStateId": "39acc89e-bd3f-4c0a-9a98-9a99f318e612"
  },
  {
    "trafficLightObjectId": "5c7a7f2c-1ef5-49fe-9555-b81e2fd4db2f",
    "date": "28.06.2021",
    "time": "11:16:31",
    "number": 1,
    "name": "T1R1",
    "state": "OK",
    "alarm": 0,
    "amperage": 0,
    "voltage": 0,
    "power": 107,
    "calibre": 0,
    "level1": 5,
    "level2": 0,
    "typeStateId": "39acc89e-bd3f-4c0a-9a98-9a99f318e612"
  }
];
const PeekStmodLogs = [
  {
    "trafficLightObjectId": "5c7a7f2c-1ef5-49fe-9555-b81e2fd4db2f",
    "date": "28.06.2021",
    "time": "14:44:37",
    "number": 2,
    "name": "UG405_OTU-1",
    "state": "OK",
    "cause": 0,
    "counter": 1,
    "other": 0,
    "typeStateId": "39acc89e-bd3f-4c0a-9a98-9a99f318e612"
  },
  {
    "trafficLightObjectId": "5c7a7f2c-1ef5-49fe-9555-b81e2fd4db2f",
    "date": "28.06.2021",
    "time": "14:44:37",
    "number": 3,
    "name": "MMI-1",
    "state": "OK",
    "cause": 0,
    "counter": 0,
    "other": 1,
    "typeStateId": "39acc89e-bd3f-4c0a-9a98-9a99f318e612"
  },
  {
    "trafficLightObjectId": "5c7a7f2c-1ef5-49fe-9555-b81e2fd4db2f",
    "date": "28.06.2021",
    "time": "14:38:00",
    "number": 2,
    "name": "UG405_OTU-1",
    "state": "OK",
    "cause": 0,
    "counter": 1,
    "other": 0,
    "typeStateId": "39acc89e-bd3f-4c0a-9a98-9a99f318e612"
  },
  {
    "trafficLightObjectId": "5c7a7f2c-1ef5-49fe-9555-b81e2fd4db2f",
    "date": "28.06.2021",
    "time": "14:38:00",
    "number": 3,
    "name": "MMI-1",
    "state": "OK",
    "cause": 0,
    "counter": 0,
    "other": 1,
    "typeStateId": "39acc89e-bd3f-4c0a-9a98-9a99f318e612"
  },
  {
    "trafficLightObjectId": "5c7a7f2c-1ef5-49fe-9555-b81e2fd4db2f",
    "date": "28.06.2021",
    "time": "14:31:57",
    "number": 1,
    "name": "SOLAR-1",
    "state": "OK",
    "cause": 0,
    "counter": 0,
    "other": 0,
    "typeStateId": "39acc89e-bd3f-4c0a-9a98-9a99f318e612"
  },
  {
    "trafficLightObjectId": "5c7a7f2c-1ef5-49fe-9555-b81e2fd4db2f",
    "date": "28.06.2021",
    "time": "14:31:57",
    "number": 2,
    "name": "UG405_OTU-1",
    "state": "OK",
    "cause": 0,
    "counter": 1,
    "other": 0,
    "typeStateId": "39acc89e-bd3f-4c0a-9a98-9a99f318e612"
  },
  {
    "trafficLightObjectId": "5c7a7f2c-1ef5-49fe-9555-b81e2fd4db2f",
    "date": "28.06.2021",
    "time": "14:31:57",
    "number": 3,
    "name": "MMI-1",
    "state": "OK",
    "cause": 0,
    "counter": 0,
    "other": 1,
    "typeStateId": "39acc89e-bd3f-4c0a-9a98-9a99f318e612"
  },
  {
    "trafficLightObjectId": "5c7a7f2c-1ef5-49fe-9555-b81e2fd4db2f",
    "date": "28.06.2021",
    "time": "11:40:48",
    "number": 2,
    "name": "UG405_OTU-1",
    "state": "OK",
    "cause": 0,
    "counter": 1,
    "other": 0,
    "typeStateId": "39acc89e-bd3f-4c0a-9a98-9a99f318e612"
  },
  {
    "trafficLightObjectId": "5c7a7f2c-1ef5-49fe-9555-b81e2fd4db2f",
    "date": "28.06.2021",
    "time": "11:40:48",
    "number": 3,
    "name": "MMI-1",
    "state": "OK",
    "cause": 0,
    "counter": 0,
    "other": 1,
    "typeStateId": "39acc89e-bd3f-4c0a-9a98-9a99f318e612"
  },
  {
    "trafficLightObjectId": "5c7a7f2c-1ef5-49fe-9555-b81e2fd4db2f",
    "date": "28.06.2021",
    "time": "11:21:46",
    "number": 2,
    "name": "UG405_OTU-1",
    "state": "OK",
    "cause": 0,
    "counter": 1,
    "other": 0,
    "typeStateId": "39acc89e-bd3f-4c0a-9a98-9a99f318e612"
  },
  {
    "trafficLightObjectId": "5c7a7f2c-1ef5-49fe-9555-b81e2fd4db2f",
    "date": "28.06.2021",
    "time": "11:21:46",
    "number": 3,
    "name": "MMI-1",
    "state": "OK",
    "cause": 0,
    "counter": 0,
    "other": 1,
    "typeStateId": "39acc89e-bd3f-4c0a-9a98-9a99f318e612"
  },
  {
    "trafficLightObjectId": "5c7a7f2c-1ef5-49fe-9555-b81e2fd4db2f",
    "date": "28.06.2021",
    "time": "11:16:32",
    "number": 1,
    "name": "SOLAR-1",
    "state": "OK",
    "cause": 0,
    "counter": 0,
    "other": 0,
    "typeStateId": "39acc89e-bd3f-4c0a-9a98-9a99f318e612"
  },
  {
    "trafficLightObjectId": "5c7a7f2c-1ef5-49fe-9555-b81e2fd4db2f",
    "date": "28.06.2021",
    "time": "11:16:32",
    "number": 2,
    "name": "UG405_OTU-1",
    "state": "OK",
    "cause": 0,
    "counter": 1,
    "other": 0,
    "typeStateId": "39acc89e-bd3f-4c0a-9a98-9a99f318e612"
  },
];
const PeekStsgLogs = [
  {
    "trafficLightObjectId": "b46b2479-26cd-46cb-a3f1-a3eea651fe6b",
    "date": "28.06.2021",
    "time": "14:52:11",
    "number": 0,
    "name": null,
    "state": "Ok",
    "timeState": 0,
    "signal": null,
    "timeSignal": 0,
    "other": 0,
    "typeStateId": "00000000-0000-0000-0000-000000000000"
  },
  {
    "trafficLightObjectId": "b46b2479-26cd-46cb-a3f1-a3eea651fe6b",
    "date": "28.06.2021",
    "time": "14:47:08",
    "number": 0,
    "name": null,
    "state": "Error",
    "timeState": 0,
    "signal": null,
    "timeSignal": 0,
    "other": 0,
    "typeStateId": "00000000-0000-0000-0000-000000000000"
  },
  {
    "trafficLightObjectId": "b46b2479-26cd-46cb-a3f1-a3eea651fe6b",
    "date": "28.06.2021",
    "time": "14:42:06",
    "number": 0,
    "name": null,
    "state": "Ok",
    "timeState": 0,
    "signal": null,
    "timeSignal": 0,
    "other": 0,
    "typeStateId": "00000000-0000-0000-0000-000000000000"
  },
  {
    "trafficLightObjectId": "b46b2479-26cd-46cb-a3f1-a3eea651fe6b",
    "date": "28.06.2021",
    "time": "14:37:01",
    "number": 0,
    "name": null,
    "state": "Error",
    "timeState": 0,
    "signal": null,
    "timeSignal": 0,
    "other": 0,
    "typeStateId": "00000000-0000-0000-0000-000000000000"
  },
  {
    "trafficLightObjectId": "b46b2479-26cd-46cb-a3f1-a3eea651fe6b",
    "date": "28.06.2021",
    "time": "14:31:55",
    "number": 0,
    "name": null,
    "state": "Error",
    "timeState": 0,
    "signal": null,
    "timeSignal": 0,
    "other": 0,
    "typeStateId": "00000000-0000-0000-0000-000000000000"
  },
  {
    "trafficLightObjectId": "b46b2479-26cd-46cb-a3f1-a3eea651fe6b",
    "date": "28.06.2021",
    "time": "14:21:54",
    "number": 0,
    "name": null,
    "state": "Error",
    "timeState": 0,
    "signal": null,
    "timeSignal": 0,
    "other": 0,
    "typeStateId": "00000000-0000-0000-0000-000000000000"
  },
  {
    "trafficLightObjectId": "b46b2479-26cd-46cb-a3f1-a3eea651fe6b",
    "date": "28.06.2021",
    "time": "14:15:51",
    "number": 0,
    "name": null,
    "state": "Ok",
    "timeState": 0,
    "signal": null,
    "timeSignal": 0,
    "other": 0,
    "typeStateId": "00000000-0000-0000-0000-000000000000"
  },
];
const PeekStsysLog = [
  {
    "trafficLightObjectId": "cbad1a56-ac28-46d5-96fd-f7fc70e4634c",
    "date": "01.01.0001",
    "time": "03:00:00",
    "loadCPU_R": 10,
    "loadCPU_L": 1,
    "power": "255 В / 50.0 Hz",
    "power2": 1,
    "tempCPU": 8,
    "voltagePower": 230,
    "voltageCPU": 332
  },
  {
    "trafficLightObjectId": "cbad1a56-ac28-46d5-96fd-f7fc70e4634c",
    "date": "01.01.0001",
    "time": "03:00:00",
    "loadCPU_R": 10,
    "loadCPU_L": 1,
    "power": "244 В / 50.0 Hz",
    "power2": 0,
    "tempCPU": 31,
    "voltagePower": 230,
    "voltageCPU": 332
  },
  {
    "trafficLightObjectId": "cbad1a56-ac28-46d5-96fd-f7fc70e4634c",
    "date": "01.01.0001",
    "time": "03:00:00",
    "loadCPU_R": 10,
    "loadCPU_L": 1,
    "power": "254 В / 50.0 Hz",
    "power2": 0,
    "tempCPU": 23,
    "voltagePower": 230,
    "voltageCPU": 334
  },
  {
    "trafficLightObjectId": "cbad1a56-ac28-46d5-96fd-f7fc70e4634c",
    "date": "01.01.0001",
    "time": "03:00:00",
    "loadCPU_R": 10,
    "loadCPU_L": 0,
    "power": "249 В / 50.0 Hz",
    "power2": 0,
    "tempCPU": 24,
    "voltagePower": 230,
    "voltageCPU": 332
  },
];
//

const attrNames = {
  "number": "Номер",
  "name": "Название",
  "state": "Состояние",
  "alarm": "Сигнал",
  "amperage": "Ток",
  "voltage": "Напряжение",
  "power": "Мощность",
  "calibre": "Калибр",
  "level1": "Уровень1",
  "level2": "Уровень2",
  "cause": "Причина",
  "counter": "Счетчик",
  "other": "Другое",
  "signal": "Сигнал",
  "timeState": "Время состояния",
  "timeSignal": "Время сигнала",
  "loadCPU_R": "Загрузка ЦП R",
  "loadCPU_L": "Загрузка ЦП L",
  "power2": "Мощность 2",
  "tempCPU": "tempCPU",
  "voltagePower": "voltagePower",
  "voltageCPU": "напряжение ЦП"
}

const typeNames = {
  "PeekPl": "Общий",
  "PeekStlamp": "Ламп",
  "PeekStmod": "Модулей",
  "PeekStsg": "Сигнальных групп",
  "PeekStsys": "Состояний системы",
}

const columns = [
  { field: 'id', headerName: '№', width: 100 },
  { field: 'date', headerName: 'Дата', width: 100, sortable: false },
  { field: 'time', headerName: 'Время', width: 100, sortable: false },
  { field: 'type', headerName: 'Тип лога', width: 160 },
  { field: 'info', headerName: 'Информация', flex: 1, sortable: false },
];

const sortByDateTime = (a, b) => {
  let d1 = `${a.date} ${a.time}`;
  let d2 = `${b.date} ${b.time}`;
  return moment(d1, "DD.MM.YYYY hh:mm:ss") - moment(d2, "DD.MM.YYYY hh:mm:ss");
}

export default function LogsTable(props) {
  const {id} = props;
  const [logsRows, setLogsRows] = useState();
  const [error, setError] = useState(false);

  useEffect(() => {
    if (id) {
      const urls = Object.values(dictionary.logs).map(f => f(id));
      const requests = urls.map(url => axios.get(url));
      let logs = [];
      axios.all(requests).then(axios.spread((...responses) => {
        responses.forEach(el => logs = logs.concat(el.data.data.items));
        console.log("logs", logs);
        logs.sort(sortByDateTime);
        createRows(logs);
        setError(false);
      })).catch(errors => {
        console.log("errors", errors);
        setError(true);
      })
    }

    // let test1 = PeekPlLogs.map(el => {el.type = "PeekPlLogs"; return el});
    // let test2 = PeekStlampLogs.map(el => {el.type = "PeekStlampLogs"; return el});
    // let test3 = PeekStmodLogs.map(el => {el.type = "PeekStmodLogs"; return el});
    // let test4 = PeekStsgLogs.map(el => {el.type = "PeekStsgLogs"; return el});
    // let test5 = PeekStsysLog.map(el => {el.type = "PeekStsysLog"; return el});
    // let test = test1.concat(test2).concat(test3).concat(test4).concat(test5);

    // test.sort(sortByDateTime);
    // createRows(test);
    return
  }, [id])  

  const getInfo = (item) => {
    let info = "";
    for (var attr in item) {
      if (attrNames[attr]) {
        info += `${attrNames[attr]}: ${item[attr]} `;
      }
    }
    return info;
  }

  const createRows = (logs) => {
    var rows = [];
    logs.forEach((el, i) => {
      rows.push({id: i+1, date: el.date, time: el.time, type: typeNames[el.logType], info: getInfo(el)})
    });
    setLogsRows(rows);
  }

  return (
    <div className='page'>
      { error ? <p>Ошибка</p>
      : logsRows ? <DataGrid rows={logsRows} columns={columns} pageSize={50} />
        : <p>Введите ID светофорного объекта</p> }
    </div>
  )
}

LogsTable.propTypes = {
    id: PropTypes.string,
}