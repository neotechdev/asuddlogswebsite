class Dictionary {
    logs = {
        getPeekPlLogs: (id) => `http://10.1.3.58:7125/api/TrafficLightObjectLogs/PeekPlLogs?id=${id}`,
        getPeekStlampLogs: (id) => `http://10.1.3.58:7125/api/TrafficLightObjectLogs/PeekStlampLogs?id=${id}`,
        getPeekStmodLogs: (id) => `http://10.1.3.58:7125/api/TrafficLightObjectLogs/PeekStmodLogs?id=${id}`,
        getPeekStsgLogs: (id) => `http://10.1.3.58:7125/api/TrafficLightObjectLogs/PeekStsgLogs?id=${id}`,
        getPeekStsysLogs: (id) => `http://10.1.3.58:7125/api/TrafficLightObjectLogs/PeekStsysLogs?id=${id}`,
    }    
}

let dictionary = new Dictionary();

export default dictionary;